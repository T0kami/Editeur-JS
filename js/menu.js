/* ---------------------------------------------

           Gestion des onglets du menu

--------------------------------------------- */


$(document).ready(function(){

  // On stock l'élement boite à outils, les onglets et la largeur de la fenêtre
  var toolBox = $("#tool_box");
  var tabs = $(".togglable-tab");
  var windowWidth = $(window).width();;


  /*------------------------------------
  Menu pour l'interface Tablette / PC
  -------------------------------------*/

  if (windowWidth > 1000){

    // Gestion du clic sur un bouton de la boite à outils
    $(".togglable-btn").click(function(event){

      //Tout les boutons retrouvent leur style de base et le bouton cliqué change de bordures
      $(".togglable-btn").css('border', '2px solid #FC8314');
      $(this).css('border', '2px solid white');

      // On enregistre l'onglet associé au bouton cliqué dans une variable
      var tab = $('div[tab="'+ $(this).attr("tab") +'"]');
      tabs.hide();
      tab.show();

    });


  /*------------------------------------
       Menu pour l'interface Mobile
  -------------------------------------*/

  }else{
    // Gestion du clic sur un bouton de la toolbox
    $(".togglable-btn").click(function(event){

      $(".togglable-btn").css('border', '2px solid #FC8314');
      var tab = $('div[tab="'+ $(this).attr("tab") +'"]');

      // Permet de cacher à nouveau l'onglet au clic sur le bouton
      if (tab.is(":hidden")){
        tabs.hide();
        tab.show();
        $(this).css('border', '2px solid white');
      }else{
        tab.hide();
      }

    });
  }
});
