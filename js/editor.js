/* ---------------------------------------------

         Fonctions de l'éditeur de texte

--------------------------------------------- */


$(document).ready(function(){


  /* Inserer des BR à la place d'un DIV au retour de ligne */

  $('div[contenteditable]').keydown(function(e) {

    // Si la touche pressée = Entrer
    if (e.keyCode === 13) {
      document.execCommand('insertHTML', false, '<br><br>');

      // Évite le comportement par défaut
      return false;
    }
  });

  /* Sépare les arguments des commandes envoyées par l'éditeur */

  function processCommand(args){
    return args.split(" ");
  }

  /* Vérifie le nombre d'arguments d'une commande */
  function checkArgsNumber(number, args){
    if (args.length == number){
      return true;
    }
    else{
      console.log("Mauvais nombre d'arguments !");
      return false;
    }
  }

  /* Commandes des balises "select" */

  $('select, input[type="color"]').change(function(event){

    var value = $(this).val();
    var command = $(this).attr("command");
    var args = processCommand(command);

    // Si le nombre d'arguments est correct, execution de commande
    if (checkArgsNumber(2, args)){
      document.execCommand(args[0], args[1], value);
    }

  });

  /* Commandes basiques des boutons */

  $('[command]').click(function(event){
    var attr = $(this).attr("command");
    document.execCommand(attr);
  });

  /* Ajout de couleurs */

  $('.color').click(function(event){
    var command = $(this).attr("command");
    var args = processCommand(command);
    var color = $(this).css("color");

    document.execCommand(args[0], false, color);

  });

  /* Ajout de lien */

  $('.link').click(function(event){
    var attr = $(this).attr("command");
    var link = prompt("Entrez l'URL du lien");
    document.execCommand(attr, false, link);
  });


  /* Ajout d'images */

  $('#insertImg').change(function() {
    var input = document.getElementById("insertImg");
    var fReader = new FileReader();
    fReader.readAsDataURL(input.files[0]);
    fReader.onloadend = function(event){
      document.execCommand("insertImage", false, event.target.result);

    }
  });

  /* Ajout de smileys */

  $('img[alt="smiley"]').click(function() {
    var image = $(this).attr("src");
    document.execCommand("insertImage", false, image);

  });

  /* Modal */

  // Ouvrir la modal
  $("#myBtn").click(function() {
      $("#myModal").css("display", "block");
      var html = $("div[contenteditable]").html();
      $(".modal-content").html(html);
  });


  // Fermer la modal
  var modal = document.getElementById('myModal');
  $(window).click(function(event) {
      if (event.target == modal) {
        $("#myModal").css("display", "none");
      }
  });
});
